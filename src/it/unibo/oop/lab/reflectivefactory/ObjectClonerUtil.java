package it.unibo.oop.lab.reflectivefactory;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Utility class for cloning objects via reflection.
 * 
 */
public final class ObjectClonerUtil {

    private static final String GETTER_PREFIX = "get";
    private static final String SETTER_PREFIX = "set";
    private static final String ERROR_STRING = "Something gone wrong";

    private ObjectClonerUtil() {

    }

    /**
     * Clone the object provided in input.
     * 
     * @param <T>
     *            the parametric class of the object to clone
     * @param obj
     *            the object to be cloned
     * @param theClass
     *            the class of the object to clone
     * @return the cloned object
     * @throws CloningException
     *             in case of any problem during the cloning
     */
    public static <T> T cloneObj(final T obj, final Class<T> theClass) throws CloningException {
        try {
        /*
         * 1) Retrieve the constructor from theClass
         */
            final T returnValue = theClass.newInstance();
       /*
         * 2) Cycle all the methods found in theClass. If the method is a getter (starts
         * with GETTER_PREFIX) of the current class (i.e. is not a getter defined in a
         * super class: use the getDeclaringClass method for this check) find the
         * corresponding setter, keeping in mind that: - The setter name can be
         * constructed by a combination of:
         * 
         * 1)SETTER_PREFIX and
         * 
         * 2) a substring of the getter method name.
         * 
         * Note: the getReturnType method of the class Method returns the return type of
         * a method
         */
            final List<Method> getterMethods = Arrays.asList(theClass.getMethods()).stream()
                    .filter(m -> m.getDeclaringClass().equals(theClass) && m.getName().startsWith(GETTER_PREFIX))
                    .collect(Collectors.toList());
        /*
         * 3) Invoke the getter on the original object, obtain the value, and pass it to
         * the corresponding setter of the cloned object.
         */
            getterMethods.stream().forEach(m -> {
                            try {
                                theClass.getMethod(m.getName().replace(GETTER_PREFIX, SETTER_PREFIX), m.getReturnType())
                                        .invoke(returnValue, m.invoke(obj));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
            });
        /*
         * 6) Return the cloned object
         */
            return returnValue;

        } catch (InstantiationException | IllegalAccessException e) {
            throw new CloningException(ERROR_STRING);
        }
    }

}