package it.unibo.oop.lab.reflectivefactory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

/**
 * Used to test reflection on ClonableClass.
 * 
 */
public class TestCloner {

    private static final String A = "iPhone";
    private static final String B = "7";
    private static final double D = 450.0;

    /**
     * Used to test reflection on a ClonableClass instance.
     * 
     */
    @Test
    public void testInstanceCloning() {
        /*
         * 1) Create an instance of ClonableClass
         */
        final ClonableClass clonable = new ClonableClass();
        /*
         * 2) Invoke the setters to configure the object
         */
        clonable.setA(A);
        clonable.setB(B);
        clonable.setD(D);
        /*
         * 3) Clone the object leveraging ObjectClonerUtil.cloneObj
         */
        try {
            final ClonableClass cloned = ObjectClonerUtil.cloneObj(clonable, ClonableClass.class);
        /*
        * 4) Use the getters to test the equivalence of the two objects
        */
            assertEquals(A, cloned.getA());
            assertEquals(B, cloned.getB());
            assertEquals(Double.valueOf(D), cloned.getD());
        } catch (CloningException e) {
            fail();
        }
    }
}
